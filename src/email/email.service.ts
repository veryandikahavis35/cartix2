import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { options } from 'joi';
import { Users } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
const nodemailer = require('nodemailer')


const transporter = nodemailer.createTransport({
    service: 'gmail'
})

@Injectable()
export class EmailService {
    constructor(
        @InjectRepository(Users) private usersRepository: Repository<Users>,
        private mailerService: MailerService,
    ) {}


    async email(id: string) {
        const account = await this.usersRepository.find({
            where: {id}
        })
        
        const arr = []
        account.map(result => arr.push(result.email)) 
        
        this.mailerService.sendMail({
            to: arr,
            from: '"Cartix" <cartixofficial@hotmail.com>',
            subject: 'Ticket',
            text: 'tiket masuk',
            html: '<b>Hello world?</b>'
        }).then((success) => {
            console.log(success);
        })
        .catch((err) => {
            console.log(err);
        })
    }
}
