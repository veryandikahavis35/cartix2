import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from 'src/users/entities/user.entity';
import { EmailController } from './email.controller';
import { EmailService } from './email.service';

@Module({
  imports: [
  TypeOrmModule.forFeature([Users])
],
  controllers: [EmailController],
  providers: [EmailService]
})
export class EmailModule {}
