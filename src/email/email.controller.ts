import { Controller, Get, Param, ParseUUIDPipe, Post } from '@nestjs/common';
import { EmailService } from './email.service';

@Controller('email')
export class EmailController {
    constructor(private readonly emailService: EmailService) {}

    @Get('send/:id')
    email(@Param('id', ParseUUIDPipe) id: string) {
        return this.emailService.email(id)
    }
}
