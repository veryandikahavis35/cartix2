import { Tickets } from "src/events/entity/ticket.entity";
import { Users } from "src/users/entities/user.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Carts } from "./cart.entity";
import { PaymentsTickets } from "./payment.ticket.entity";

@Entity()
export class Orders{
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'enum',
        enum: ["Dipesan","Approved","Rejected"],
        default: 'Dipesan'
    })
    status: string;

    @Column({
        type: 'timestamp'
    })
    order_time: Date

    @Column()
    quantity: number
    
    @Column()
    total_price: number

    @ManyToOne(() => Tickets, ticket => ticket.order)
    ticket: Tickets

    @ManyToOne(() => Users, user => user.order)
    user: Users

    @OneToMany(() => Carts, cart => cart.order)
    cart: Carts

    @OneToMany(() => PaymentsTickets, paymentTicket => paymentTicket.order)
    paymentTicket: PaymentsTickets

    @CreateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    createdAt: Date;
    
    @UpdateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    updatedAt: Date;
    
    @DeleteDateColumn({
        type: 'timestamp',
        nullable: true,
    })
    deletedAt: Date;
}
