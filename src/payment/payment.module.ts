import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tickets } from 'src/events/entity/ticket.entity';
import { Users } from 'src/users/entities/user.entity';
import { Carts } from './entity/cart.entity';
import { Orders } from './entity/order.entity';
import { PaymentsEo } from './entity/paymen.entity';
import { PaymentsTickets } from './entity/payment.ticket.entity';
import { PaymentController } from './payment.controller';
import { CronPaymentService } from './payment.cron';
import { PaymentService } from './payment.service';

@Module({
  imports: [TypeOrmModule.forFeature([PaymentsEo, PaymentsTickets,  Users, Carts, Tickets, Orders])],
  controllers: [PaymentController],
  providers: [PaymentService, CronPaymentService]
})
export class PaymentModule {}
