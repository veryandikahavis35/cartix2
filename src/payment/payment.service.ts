import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Tickets } from 'src/events/entity/ticket.entity';
import { Users } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { ApprovedDto } from './dto/approved.dto';
import { OrdersDto } from './dto/order.dto';
import { PaymentDto } from './dto/payment.dto';
import { PaymentEoDto } from './dto/payment.eo.dto';
import { RejectedDto } from './dto/rejected.dto';
import { Carts } from './entity/cart.entity';
import { Orders } from './entity/order.entity';
import { PaymentsEo } from './entity/paymen.entity';
import { PaymentsTickets } from './entity/payment.ticket.entity';

@Injectable()
export class PaymentService {
    constructor(
        @InjectRepository(PaymentsEo) private paymentRepository: Repository<PaymentsEo>,
        @InjectRepository(Users) private usersRepository: Repository<Users>,
        @InjectRepository(Tickets) private ticketsRepository: Repository<Tickets>,
        @InjectRepository(Carts) private cartsRepository: Repository<Carts>,
        @InjectRepository(Orders) private ordersRepository: Repository<Orders>,
        @InjectRepository(PaymentsTickets) private paymentsTicketRepository: Repository<PaymentsTickets>
    ) {}

    async paymentEo(id: string, paymentEoDto: PaymentEoDto) {
        const user = await this.usersRepository.findOneOrFail({where: {id}})
        const result = new PaymentsEo()
        result.name = paymentEoDto.name
        result.phone_number = paymentEoDto.phone_number
        result.proof_of_payment = paymentEoDto.proof_of_payment
        result.user = user
        await this.paymentRepository.insert(result)
        return this.paymentRepository.findOneOrFail({where: {user: {id}}})
    }

    paymentAll() {
        return this.paymentsTicketRepository.findAndCount()
    }

    async cart(id_user: string, id_order: string) {
        try{
            const user = await this.usersRepository.findOneOrFail({
                where: {id: id_user}
            })
            const order = await this.ordersRepository.findOneOrFail({
                where: {id: id_order}
            })
            const result = new Carts()
            result.user = user
            result.order = order
            await this.cartsRepository.insert(result)
            return "Berhasil"
        }catch(err){
            throw new err
        }
    }

    idCart(id: string) {
        return this.cartsRepository.findAndCount({
            where: {
                user: {id}
            }
        })
    }

    async order(id_ticket: string, id_user: string, ordersDto: OrdersDto) {
            const ticket = await this.ticketsRepository.findOneOrFail({
                where: {id: id_ticket}
            })
            const arr = []
            const totalPrice = await this.ticketsRepository.find({
                where: {id: id_ticket}
            })
            totalPrice.map(result => arr.push(result.ticket_price))
            const user = await this.usersRepository.findOneOrFail({
                where: {id: id_user}
            })
            const result = new Orders()
            result.quantity = ordersDto.quantity
            result.ticket = ticket
            const total = arr[0] * result.quantity
            result.total_price = total
            result.user = user
            await this.ordersRepository.insert(result)
            return "berhasil"
    }

    orderUserId(id_user: string) {
        return this.ordersRepository.find({
            where: {
                user: {id: id_user}
            },
            relations: {
                user: true
            }
        })
    }

    orderEventId(id: string) {
        return this.ordersRepository.find({
            relations: {
                ticket: {
                    event: true
                }
            },
            where: {
                ticket: {
                    event: {id}
                }
            }
        })
    }

    async approved(id_order: string, id_ticket: string, approvedDto: ApprovedDto) {
        const order = await this.ordersRepository.findOneOrFail({
            where: {id: id_order}
        })
        const totalTickets = await this.ticketsRepository.findOneOrFail({
            where: {id: id_ticket}
        })
        const ticket = await this.ticketsRepository.find({
            where: {id: id_ticket}
        })
        const amount = await this.ordersRepository.find({
            where: {id: id_order}
        })
        const quantity = []
        const total = []
        amount.map(result => quantity.push(result.quantity))
        ticket.map(result => total.push(result.total_tickets))
        const result = total[0] - quantity[0]
        const apprvd = approvedDto.status
        totalTickets.total_tickets = result
        order.status = apprvd
        await this.ticketsRepository.update({id: id_ticket}, totalTickets)
        await this.ordersRepository.update({id: id_order}, order)
        return this.ordersRepository.findOneOrFail({
            where: {id: id_order}
        })
    }

    async rejected(id_order: string, id_cart: string, rejectedDto: RejectedDto) {
        const order = await this.ordersRepository.findOneOrFail({
            where: {id: id_order}
        })
        await this.ordersRepository.findOneOrFail({
            where: {id: id_order}
        })
        const cart = await this.cartsRepository.findOneOrFail({
            where: {id: id_cart}
        })
        const status = rejectedDto.status
        order.status = status
        await this.ordersRepository.update({id: id_order}, order)
        await this.paymentsTicketRepository.softDelete({order: {id: id_order}})
        await this.cartsRepository.softDelete(id_cart)
        await this.ordersRepository.softDelete(id_order)
        return this.ordersRepository.findOneOrFail({
            where: {id: id_order},
            withDeleted: true
        })
    }

    async payment(id: string, paymentDto: PaymentDto) {
        try {
        const order = await this.ordersRepository.findOneOrFail({
            where: {id}
        })
        const result = new PaymentsTickets()
        result.name = paymentDto.name
        result.phone_number = paymentDto.phone_number
        result.proof_of_payment = paymentDto.proof_of_payment
        result.order = order
        await this.paymentsTicketRepository.insert(result)
        return this.paymentsTicketRepository.findOneOrFail({
            where: {
                order: {id}
            } 
        })
    }catch(err) {
        throw new err
    }
    }

    async ticketsSold() {
        const ticket = await this.ordersRepository.find()
        const arr = []
        ticket.map(result => arr.push(result.quantity))
        const sold = await arr.reduce((a, b) => a + b)
        return sold
    }

    async totalPrice(id: string) {
        const total = await this.ordersRepository.find({
            where: [{status: 'Dipesan'}, {user: {id}}]
        })
        const arr = []
        total.map(result => arr.push(result.total_price))
        const totalPrice = await arr.reduce((a, b) => a + b)
        return totalPrice
    }

    paymentEventId(id_events: string) {
        return this.paymentsTicketRepository.find({
            relations: {
                order: {
                    ticket: {
                        event: true
                    }
                }
            },
            where: {
                order: {
                    ticket: {
                        event: {id: id_events}
                    }
                }
            }
        })
    }

    paymentOrderId(id: string) {
        return this.paymentsTicketRepository.findAndCount({
            where: {
                order: {id}
            }
        })
    }

    deleteDataOrders() {
        return this.ordersRepository.softDelete({status: 'Dipesan'})
    }

}
