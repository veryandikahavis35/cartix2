import { Body, Controller, Get, Param, ParseUUIDPipe, Post, Put } from '@nestjs/common';
import { ApprovedDto } from './dto/approved.dto';
import { OrdersDto } from './dto/order.dto';
import { PaymentDto } from './dto/payment.dto';
import { PaymentEoDto } from './dto/payment.eo.dto';
import { RejectedDto } from './dto/rejected.dto';
import { PaymentService } from './payment.service';

@Controller('payment')
export class PaymentController {
    constructor(private readonly paymentService: PaymentService) {}
    
    // ================================PAYMENT=============================
    @Post('tickets/:id')
    payment(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() paymentDto: PaymentDto
    ) {
        return this.paymentService.payment(id, paymentDto)
    }

    @Post('eo/:id')
    paymentEo(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() paymentEoDto: PaymentEoDto
    ) {
        return this.paymentService.paymentEo(id, paymentEoDto)
    }

    @Get('all')
    paymentAll() {
        return this.paymentService.paymentAll()
    }

    @Get('event/:id_events')
    paymentEventId(
        @Param('id_events', ParseUUIDPipe) id_events: string
    ) {
        return this.paymentService.paymentEventId(id_events)
    }

    @Get('order/:id')
    paymentOrderId(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.paymentService.paymentOrderId(id)
    }

    @Get('total/price/:id')
    totalPrice(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.paymentService.totalPrice(id)
    }

    // ============================================CART===============================
    @Post('cart/:id_user/:id_order')
    cart(
        @Param('id_user', ParseUUIDPipe) id_user: string,
        @Param('id_order', ParseUUIDPipe) id_order: string
    ) {
        return this.paymentService.cart(id_user, id_order)
    }

    @Get('cart/:id')
    idCart(
        @Param('id',ParseUUIDPipe) id: string
    ) {
        return this.paymentService.idCart(id)
    }

    // ============================================ORDER================================
    @Post('order/:id_ticket/:id_user')
    order(
        @Param('id_ticket', ParseUUIDPipe) id_ticket: string,
        @Param('id_user', ParseUUIDPipe) id_user: string,
        @Body() ordersDto: OrdersDto
    ) {
        return this.paymentService.order(id_ticket, id_user, ordersDto)
    }

    @Get('orders/:id_user')
    orderUserId(
        @Param('id_user', ParseUUIDPipe) id_user: string
    ) {
        return this.paymentService.orderUserId(id_user)
    }

    @Get('order/event/:id')
    orderEventId(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.paymentService.orderEventId(id)
    }

    @Get('tickets/sold')
    ticketsSold() {
        return this.paymentService.ticketsSold()
    }

    @Put('order/approved/:id_order/:id_ticket')
    approved(
        @Param('id_order', ParseUUIDPipe) id_order: string,
        @Param('id_ticket', ParseUUIDPipe) id_ticket: string,
        @Body() approvedDto: ApprovedDto
    ) {
        return this.paymentService.approved(id_order, id_ticket, approvedDto)
    }

    @Put('order/rejected/:id_order/:id_cart')
    rejected(
        @Param('id_order', ParseUUIDPipe) id_order: string,
        @Param('id_cart', ParseUUIDPipe) id_cart: string,
        @Body() rejectedDto: RejectedDto
    ) {
        return this.paymentService.rejected(id_order, id_cart, rejectedDto)
    }

}