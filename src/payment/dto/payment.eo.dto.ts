import { IsNotEmpty } from "class-validator";

export class PaymentEoDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    phone_number: string;

    @IsNotEmpty()
    proof_of_payment: string;
}