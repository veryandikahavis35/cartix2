import { IsString } from "class-validator";

export class RejectedDto {
    
    @IsString()
    status: string = 'Rejected'
}