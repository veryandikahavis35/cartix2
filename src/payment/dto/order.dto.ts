import { IsNotEmpty } from "class-validator";

export class OrdersDto {
    @IsNotEmpty()
    quantity: number
}