import { SetMetadata } from "@nestjs/common";
// import { Roles } from "./entities/role.entity";
import { Roless } from "./role.enum";

export const ROLES_KEY = 'roles';
export const Roles = (...roles: Roless[]) => SetMetadata(ROLES_KEY, roles)