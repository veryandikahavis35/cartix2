import { Column, CreateDateColumn, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Users } from "./user.entity";

@Entity()
export class Roles {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'enum',
        enum: ["Customer","Event Organizer","Admin"],
        default: 'Customer'
    })
    role: string;

    @OneToMany(() => Users, user => user.role)
    user: Users;

    @CreateDateColumn({
        type: 'timestamp',
        nullable: false,
      })
      createdAt: Date;
    
      @UpdateDateColumn({
        type: 'timestamp',
        nullable: false,
      })
      updatedAt: Date;
    
      @DeleteDateColumn({
        type: 'timestamp',
        nullable: true,
      })
      deletedAt: Date;
}