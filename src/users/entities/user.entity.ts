import { Events } from 'src/events/entity/events.entity';
import { Reviews } from 'src/events/entity/review.entity';
import { Carts } from 'src/payment/entity/cart.entity';
import { Orders } from 'src/payment/entity/order.entity';
import { PaymentsEo } from 'src/payment/entity/paymen.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  CreateDateColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { Roless } from '../role.enum';
import { Banks } from './bank.entity';
import { Portfolio } from './portfolio.entity';
import { Roles } from './role.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  username: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  hash: string;

  @Column({nullable: true})
  poto_profile: string;

  @Column()
  phone_number: string;

  @Column({nullable: true})
  address: string;

  @Column({nullable: true})
  gender: string;

  @Column({type: 'date', nullable: true})
  date_of_birth: Date;

  @ManyToOne(() => Roles, role => role.user)
  role: number;

  roles: Roless[]

  @OneToMany(() => Banks, bank => bank.user)
  bank: Banks;

  @OneToMany(() => PaymentsEo, payment => payment.user)
  payment: PaymentsEo;

  @OneToOne(() => Portfolio, portfolio => portfolio.user)
  portfolio: Portfolio;

  @OneToMany(() => Events, event => event.user)
  event: Events;

  @OneToMany(() => Carts, cart => cart.user)
  cart: Carts

  @OneToMany(() => Orders, order => order.user)
  order: Orders

  @OneToMany(() => Reviews, review => review.user)
  review: Reviews

  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    nullable: true,
  })
  deletedAt: Date;
}
