import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Users } from "./user.entity";

@Entity()
export class Banks{
    @PrimaryGeneratedColumn('uuid')
    id: string;
    
    @Column()
    bank_name: string;

    @Column()
    bank_account: string;

    @Column()
    userbank_name: string;

    @ManyToOne(()=> Users, user => user.bank)
    user: Users;
    
    @CreateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    createdAt: Date;
    
    @UpdateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    updatedAt: Date;
    
    @DeleteDateColumn({
        type: 'timestamp',
        nullable: true,
    })
    deletedAt: Date;
}