import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Users } from "./user.entity";

@Entity()
export class Portfolio {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    portfolio: string;

    @Column()
    social_media: string;

    @OneToOne(() => Users, user => user.portfolio)
    @JoinColumn()
    user: Users;

    @CreateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    createdAt: Date;
    
    @UpdateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    updatedAt: Date;
    
    @DeleteDateColumn({
        type: 'timestamp',
        nullable: true,
    })
    deletedAt: Date;
}