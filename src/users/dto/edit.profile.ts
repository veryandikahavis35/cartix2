import { MaxLength, MinLength, IsNotEmpty, IsString, IsEmail } from "class-validator";

export class EditProfileDto{
    @MaxLength(25)
    @MinLength(2)
    @IsString()
    username: string;

    @IsEmail()
    email: string;

    @MinLength(3)
    @MaxLength(15)
    phone_number: string;

    address: string;

    gender: string;

    date_of_birth: Date;
}
