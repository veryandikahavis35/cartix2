import { IsNotEmpty } from "class-validator";

export class ChangeRoleDto {
    @IsNotEmpty()
    role: number = 2;
}