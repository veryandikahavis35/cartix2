import { IsNotEmpty } from "class-validator";

export class AddPortFolioDto {
    portfolio: string;

    @IsNotEmpty()
    social_media: string;
}