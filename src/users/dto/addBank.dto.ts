import { IsNotEmpty } from "class-validator";

export class AddBank{
    @IsNotEmpty()
    bank_name: string;

    @IsNotEmpty()
    bank_account: string;

    @IsNotEmpty()
    userbank_name: string;
}