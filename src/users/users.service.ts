import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Users } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Roles } from './entities/role.entity';
import { RoleDto } from 'src/auth/dto/role.dto';
import { EditProfileDto } from './dto/edit.profile';
import { AddBank } from './dto/addBank.dto';
import { Banks } from './entities/bank.entity';
import { AddPortFolioDto } from './dto/addPortfolio.dto';
import { Portfolio } from './entities/portfolio.entity';
import { ChangeRoleDto } from './dto/changeRole.dto';
import { ChangePasswordDto } from './dto/changePassword.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private usersRepository: Repository<Users>,
    @InjectRepository(Roles) private roleRepository: Repository<Roles>,
    @InjectRepository(Banks) private bankRepository: Repository<Banks>,
    @InjectRepository(Portfolio) private portfolioRepository: Repository<Portfolio>
  ) {}

  async insertRole(roleDto: RoleDto) {
    const result = new Roles()
    result.role = roleDto.role

    return this.roleRepository.insert(result)
  }

  findAll() {
    return this.usersRepository.findAndCount();
  }

  countUser() {
    return this.usersRepository.count()
  }

  async findOne(id: string) {
    try {
      return await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  

  async addBank(id: string, addBank: AddBank) {
    const user = await this.usersRepository.findOneOrFail({where: {id}})
    const result = new Banks()
    result.bank_name = addBank.bank_name
    result.bank_account = addBank.bank_account
    result.userbank_name = addBank.userbank_name
    result.user = user
    await this.bankRepository.insert(result)
    return this.bankRepository.findOneOrFail({where: {user: {id}}})
  }

  bankAll() {
    return this.bankRepository.findAndCount()
  }

  bankUserId(id: string) {
    return this.bankRepository.find({
      relations: {
        user: true
      },
      where: {
        user: {id}
      }
    })
  }

  async addPortfolio(id: string, addPortfolioDto: AddPortFolioDto) {
    const user = await this.usersRepository.findOneOrFail({where: {id}})
    const result = new Portfolio()
    result.portfolio = addPortfolioDto.portfolio
    result.social_media = addPortfolioDto.social_media
    result.user = user
    await this.portfolioRepository.insert(result)
    return this.portfolioRepository.findOneOrFail({where: {user: {id}}})
  }

  async editProfile(id: string, editProfileDto: EditProfileDto){
    try {
      const result = await this.usersRepository.findOneOrFail({where: {id: id}})
      result.username = editProfileDto.username
      result.email = editProfileDto.email
      result.phone_number = editProfileDto.phone_number
      result.address = editProfileDto.address
      result.gender = editProfileDto.gender
      result.date_of_birth = editProfileDto.date_of_birth
      await this.usersRepository.update({id}, result)
      return this.usersRepository.findOneOrFail({where: {id}})
    }catch(err){
      throw err
    }
  }

  // async changePassword(id: string, changePasswordDto: ChangePasswordDto) {
  //   const newPassword = changePasswordDto.
  // }

  async changeRole(id: string, changeRoleDto: ChangeRoleDto) {
    const user = await this.usersRepository.findOneOrFail({where: {id}})
    // user.role = changeRoleDto.role
    await this.usersRepository.update({id}, user)
    return this.usersRepository.findOneOrFail({where: {id}, relations: {role: true}})
  }

  async remove(id: string) {
    try {
      await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
    await this.usersRepository.delete(id);
  }

  async bankDelete(id: string) {
    try{
      await this.bankRepository.findOneOrFail({
        where: {id}
      })
    }catch (err) {
      if (err instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND
        )
      } else {
        throw err
      }
    }
    return this.bankRepository.softDelete(id)
  }
}
