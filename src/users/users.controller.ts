import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ParseUUIDPipe,
  HttpStatus,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { RoleDto } from 'src/auth/dto/role.dto';
import { EditProfileDto } from './dto/edit.profile';
import { AddBank } from './dto/addBank.dto';
import { AddPortFolioDto } from './dto/addPortfolio.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { ChangeRoleDto } from './dto/changeRole.dto';
import multer from 'multer';
import { ChangePasswordDto } from './dto/changePassword.dto';


@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('bank/:id')
  async addBank(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() addBank: AddBank
  ) {
    return this.usersService.addBank(id, addBank)
  }

  @Get('bank')
  bankAll() {
    return this.usersService.bankAll()
  }

  @Get('bank/user/:id')
  bankUserId(
    @Param('id', ParseUUIDPipe) id: string
  ) {
    return this.usersService.bankUserId(id)
  }

  @Post('portfolio/:id')
  async addPortfolio(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() addPortfolioDto: AddPortFolioDto
  ) {
    return this.usersService.addPortfolio(id, addPortfolioDto)
  }

  @Post('portfolio/upload/file')
  @UseInterceptors(FileInterceptor('file'))
  async uploadPortfolioDoc(@UploadedFile() file: Express.Multer.File) {
    console.log('file', file);
    
    return ''
  }

  @Get()
  async findAll() {
    const [data, count] = await this.usersService.findAll();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.usersService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('count/all')
  countUser() {
    return this.usersService.countUser()
  }

  @Put('edit/:id')
  editProfile(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() editProfileDto: EditProfileDto
  ){
    return this.usersService.editProfile(id, editProfileDto)
  }

  @Put('change/password/:id')
  changePassword(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() changePasswordDto: ChangePasswordDto
  ) {
    
  }

  @Put('change/eo/:id')
  changeRole(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() changeRoleDto: ChangeRoleDto
  ) {
    return this.usersService.changeRole(id, changeRoleDto)
  }

  @Delete(':id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.usersService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete('bank/:id')
  async bankDelete(
    @Param('id', ParseUUIDPipe) id: string
  ) {
    await this.usersService.bankDelete(id)
    return {
      statusCode: HttpStatus.OK,
      message: 'success'
    }
  }
}
