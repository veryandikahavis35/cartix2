import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs";
import { Roles } from "./entities/role.entity";
import { ROLES_KEY } from "./role.decorator";
import { Roless } from "./role.enum";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean {
        const requiredRoles = this.reflector.getAllAndOverride<Roless[]>(ROLES_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        if (!requiredRoles) {
            return true
        } {
            const { Users } = context.switchToHttp().getRequest()
            return requiredRoles.some((roles) => Users.roles?.includes(roles))
        }
    }
}