import { HttpException, Injectable } from '@nestjs/common';
import { generateString, InjectRepository } from '@nestjs/typeorm';
import { hashPassword } from 'helper/hash_password';
import { Roles } from 'src/users/entities/role.entity';
import { Users } from 'src/users/entities/user.entity';
import { IsNull, Repository } from 'typeorm';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { RoleDto } from './dto/role.dto';
import { JwtService } from '@nestjs/jwt'

@Injectable()
export class AuthService {
    constructor (
        @InjectRepository(Users) private userRepository: Repository<Users>,
        @InjectRepository(Roles) private rolesRepository: Repository<Roles>,
        private jwtService: JwtService
    ) {}

    async register(registerDto: RegisterDto) {
        try{
            const hash = generateString();
            const password = await hashPassword(registerDto.password, hash)
            const result = new Users()
            result.username = registerDto.username
            result.email = registerDto.email
            result.password = password
            result.hash = hash
            result.phone_number = registerDto.phone_number
            result.role = 1

            return this.userRepository.insert(result)
        }catch(err){
            throw err
        }
    }


    async login(request: LoginDto) {
        try {
            const existUser = await this.userRepository.findOne({
                relations: {
                    role: true
                },
                where: {
                    username: request.username,
                    deletedAt: IsNull()
                }
            });
            const {password, hash, ...query} = existUser;
            const accessToken = this.jwtService.sign({
                query
            });
            if (existUser && existUser.password === await hashPassword(request.password, existUser.hash)) {
                return accessToken;
            } else {
                throw new HttpException ({
                    statusCode : 400,
                    message: 'Bad Request',
                    data: 'Username or password is wrong'                   
                }, 400)
            }
        } catch (e){
            throw e
        }
    }
}
