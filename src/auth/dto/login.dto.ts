import { MaxLength, MinLength, IsNotEmpty, IsString } from "class-validator";

export class LoginDto{
    @MaxLength(25)
    @MinLength(2)
    @IsNotEmpty()
    @IsString()
    username: string;

    @IsNotEmpty()
    @MinLength(7)
    @MaxLength(20)
    password: string;
}