import { IsEmail, IsNotEmpty, IsString, Matches, Max, MaxLength, MinLength } from "class-validator";
import { Roles } from "src/users/entities/role.entity";

export class RegisterDto{
    @MaxLength(25)
    @MinLength(2)
    @IsNotEmpty()
    @IsString()
    username: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @MinLength(7)
    @MaxLength(20)
    password: string;

    @IsNotEmpty()
    @MinLength(3)
    @MaxLength(15)
    phone_number: string;

    roles: number= 1
}