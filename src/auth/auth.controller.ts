import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { RoleDto } from './dto/role.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @Post('register')
    async register(@Body() registerDto: RegisterDto) {
        try{
            return this.authService.register(registerDto)
        }catch(err){
            throw err
        }
    }

    @Post('/login')
    async Login(@Body() request: LoginDto) {
            const res = await this.authService.login(request);
        return {
            statusCode: 200,
            message: 'Success',
            accessToken: res
        }
    }

    @Get()
    @UseGuards(AuthGuard())
    async testAuth(@Req() req) {
        console.log(req.user)
    }
}
