import { Users } from "src/users/entities/user.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Events } from "./events.entity";

@Entity()
export class Reviews {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    comment: string

    @Column()
    rating: number

    @ManyToOne(() => Users, user => user.review)
    user: Users

    @ManyToOne(() => Events, event => event.review)
    event: Events

    @CreateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    createdAt: Date;
    
    @UpdateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    updatedAt: Date;
    
    @DeleteDateColumn({
        type: 'timestamp',
        nullable: true,
    })
    deletedAt: Date;
}