import { Carts } from "src/payment/entity/cart.entity";
import { Orders } from "src/payment/entity/order.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Events } from "./events.entity";

@Entity()
export class Tickets {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    ticket_category: string;

    @Column()
    ticket_price: number;

    @Column()
    total_tickets: number;

    @ManyToOne(() => Events, event => event.ticket)
    event: Events;
    
    @OneToMany(() => Orders, order => order.ticket)
    order: Orders[]
    
    @CreateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    createdAt: Date;
    
    @UpdateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    updatedAt: Date;
    
    @DeleteDateColumn({
        type: 'timestamp',
        nullable: true,
    })
    deletedAt: Date;
}