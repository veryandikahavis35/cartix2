import { Users } from "src/users/entities/user.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Reviews } from "./review.entity";
import { Tickets } from "./ticket.entity";

@Entity()
export class Events {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    event_name: string;

    @Column()
    organizer_name: string;

    @Column({
        type: 'date'
    })
    event_date: Date;

    @Column()
    event_time: string;

    @Column()
    category: string;

    @Column()
    event_venue: string;

    @Column()
    description: string;
    
    @Column()
    detail: string;

    @Column()
    event_banner: string;

    @ManyToOne(() => Users, user => user.event)
    user: Users;

    @OneToMany(() => Tickets, ticket => ticket.event)
    ticket: Tickets[];

    @OneToMany(() => Reviews, review => review.event)
    review: Reviews

    @Column({
        type: 'enum',
        enum: ['Active','Done'],
        default: 'Active'
    })
    event_status: string;

    @CreateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    createdAt: Date;
    
    @UpdateDateColumn({
        type: 'timestamp',
        nullable: false,
    })
    updatedAt: Date;
    
    @DeleteDateColumn({
        type: 'timestamp',
        nullable: true,
    })
    deletedAt: Date;
}