import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Events } from './entity/events.entity';
import { Users } from 'src/users/entities/user.entity';
import { Tickets } from './entity/ticket.entity';
import { Reviews } from './entity/review.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Events, Users, Tickets, Reviews])],
  providers: [EventsService],
  controllers: [EventsController]
})
export class EventsModule {}
