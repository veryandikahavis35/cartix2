import { IsString } from "class-validator";

export class EventsStatusDto {
    @IsString()
    event_status: string = 'Done'
}