import { IsNotEmpty, IsString } from "class-validator";

export class ReviewsDto {
    @IsString()
    comment: string

    @IsNotEmpty()
    rating: number
}