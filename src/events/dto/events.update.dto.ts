import { Type } from "class-transformer"
import { IsNotEmpty, IsDate, IsString, IsOptional } from "class-validator"

export class EventsUpdateDto {
    @IsOptional()
    event_name: string

    @IsOptional()
    organizer_name: string

    @IsOptional()
    event_venue: string

    @IsOptional()
    description: string

    @IsOptional()
    category: string

    @IsOptional()
    detail: string

    @IsOptional()
    event_banner: string

    @IsOptional()
    @Type(() => Date)
    event_date: Date

    @IsOptional()
    event_time: string
}