import { Type } from "class-transformer";
import { IsDate, IsNotEmpty } from "class-validator";

export class EventsDto{
    @IsNotEmpty()
    event_name: string

    @IsNotEmpty()
    organizer_name: string

    @IsNotEmpty()
    event_venue: string

    @IsNotEmpty()
    description: string

    @IsNotEmpty()
    category: string

    @IsNotEmpty()
    detail: string

    @IsNotEmpty()
    event_banner: string

    @IsNotEmpty()
    @Type(() => Date)
    @IsDate()
    event_date: Date

    @IsNotEmpty()
    event_time: string

    event_status: string
}