import { IsNotEmpty } from "class-validator";

export class TicketsDto{
    @IsNotEmpty()
    ticket_category: string

    @IsNotEmpty()
    ticket_price: number

    @IsNotEmpty()
    total_tickets: number
}