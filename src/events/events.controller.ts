import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post, Put, Query, Res, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt.guard';
// import { Roles } from 'src/users/entities/role.entity';
import { Roles } from 'src/users/role.decorator';
import { Roless } from 'src/users/role.enum';
import { RolesGuard } from 'src/users/role.guard';
import { Repository } from 'typeorm';
import { EventsDto } from './dto/events.dto';
import { EventsStatusDto } from './dto/events.statusDone';
import { EventsUpdateDto } from './dto/events.update.dto';
import { ReviewsDto } from './dto/reviews.dto';
import { TicketsDto } from './dto/tickets.dto';
import { Events } from './entity/events.entity';
import { EventsService } from './events.service';

@Controller('events')
export class EventsController {
    constructor(
        private readonly eventService: EventsService
    ) {}

    // ================================EVENTS=================================
    @Post(':id')
    // @UseGuards(RolesGuard)
    // @Roles(Roless.EventsOrganizer)
    // @Role(Roless.EventsOrganizer)
    @UseGuards(JwtAuthGuard)
    events(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() eventsDto: EventsDto
    ) {
        return this.eventService.events(id, eventsDto)
    }

    @Get()
    allEvents() {
        return this.eventService.allEvents()
    }

    @Get(':event_name')
    eventsSearch(
        @Param('event_name') event_name: string
    ) {
        return this.eventService.eventsSearch(event_name)
    }

    @Get('category/:category')
    eventsSearchCategory(
        @Param('category') category: string
    ) {
        return this.eventService.eventsSearchCategory(category)
    }

    @Get(':id')
    idEvents(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.eventService.idEvents(id)
    }
    

    @Get('user/:id')
    eventsUserId(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.eventService.eventsUserId(id)
    }

    @Get('organizer/table/:id')
    eventsTable(@Param('id', ParseUUIDPipe) id: string) {
        return this.eventService.eventsTable(id)
    }

    @Get('status/active')
    eventsActive() {
        return this.eventService.eventsActive()
    }

    @Get('history/done')
    eventsHistory() {
        return this.eventService.eventsHistory()
    }

    @Get('count/all')
    countAll() {
        return this.eventService.countAll()
    }

    @Get('excel/:id')
    excel(
        @Param('id', ParseUUIDPipe) id: string,
    ) {
        return this.eventService.excel(id)
    }

    @Get('excel/download/:id')
    async excelDownload(
        @Param('id', ParseUUIDPipe) id: string,
        @Res() res
    ) {
        return res.download(
            `./uploads/excel/${await this.excel(id)}`,
            'events.xlsx'
        )
    }

    @Put('update/:id')
    eventsUpdate(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() eventsUpdateDto: EventsUpdateDto
    ) {
        return this.eventService.eventsUpdate(id, eventsUpdateDto)
    }

    @Put('status/done/:id')
    eventsStatusDone(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() eventsStatusDto: EventsStatusDto
    ) {
        return this.eventService.eventStatusDone(id, eventsStatusDto)
    }

    @Delete('delete/:id')
    eventDelete(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.eventService.eventDelete(id)
    }

    //==================================TICKETS===============================
    @Post('ticket/:id')
    ticket(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() ticketsDto: TicketsDto
    ) {
        return this.eventService.tickets(id, ticketsDto)
    }

    @Get('tickets/:id')
    ticketEventsId(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.eventService.ticketEventsId(id)
    }

    //==================================ULASAN================================
    @Post('review/:id_user/:id_event')
    review(
        @Param('id_user', ParseUUIDPipe) id_user: string,
        @Param('id_event', ParseUUIDPipe) id_event: string,
        @Body() reviewsDto: ReviewsDto
    ) {
        return this.eventService.review(id_user, id_event, reviewsDto)
    }

    @Get('reviews/all')
    reviews() {
        return this.eventService.reviews()
    }

    @Get('reviews/:id')
    reviewEventsId(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.eventService.reviewsEventsId(id)
    }

    @Get('reviews/rating/average/:id')
    reviewRatingEventsId(
        @Param('id', ParseUUIDPipe) id: string
    ) {
        return this.eventService.reviewsRatingEventsId(id)
    }
}
