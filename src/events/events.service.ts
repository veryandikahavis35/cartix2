import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { generateExcel } from 'helper/export_excel';
import { string, when } from 'joi';
import { async } from 'rxjs';
import { Users } from 'src/users/entities/user.entity';
import { EntityNotFoundError, Like, Repository } from 'typeorm';
import { EventsDto } from './dto/events.dto';
import { EventsStatusDto } from './dto/events.statusDone';
import { EventsUpdateDto } from './dto/events.update.dto';
import { ReviewsDto } from './dto/reviews.dto';
import { TicketsDto } from './dto/tickets.dto';
import { Events } from './entity/events.entity';
import { Reviews } from './entity/review.entity';
import { Tickets } from './entity/ticket.entity';

@Injectable()
export class EventsService {
    constructor(
        @InjectRepository(Events) private eventsRepository: Repository<Events>,
        @InjectRepository(Users) private usersRepository: Repository<Users>,
        @InjectRepository(Tickets) private ticketsRepository: Repository<Tickets>,
        @InjectRepository(Reviews) private reviewsRepository: Repository<Reviews>
    ) {}

    async events(id: string, eventsDto: EventsDto) {
        const user = await this.usersRepository.findOneOrFail({
            where: {id}
        })
        const result = new Events()
        result.event_name = eventsDto.event_name
        result.organizer_name = eventsDto.organizer_name
        result.event_venue = eventsDto.event_venue
        result.description = eventsDto.description
        result.detail = eventsDto.detail
        result.category = eventsDto.category
        result.event_banner = eventsDto.event_banner
        result.event_date = eventsDto.event_date
        result.event_time = eventsDto.event_time
        result.user = user
        await this.eventsRepository.insert(result)
        return this.eventsRepository.findOneOrFail({where: {user: {id}}})
    }

    allEvents() {
        return this.eventsRepository.findAndCount()
    }

    eventsSearch(event_name: string) {
        try{
            return this.eventsRepository.findBy({
                event_name: Like(`%${event_name}%`)
            })
        } catch (error) {
            if (error instanceof EntityNotFoundError) {
                throw new HttpException(
                    {
                        statusCode: HttpStatus.NOT_FOUND,
                        error: 'Data not found',
                    },
                    HttpStatus.NOT_FOUND,
                    );
                } else {
                    throw error;
                }
            }
    }

    eventsSearchCategory(category: string) {
        try{
            return this.eventsRepository.findBy({
                category: Like(`%${category}%`)
            })
        } catch(error) {
            if (error instanceof EntityNotFoundError) {
                throw new HttpException(
                    {
                        statusCode: HttpStatus.NOT_FOUND,
                        error: 'Data not found',
                    },
                    HttpStatus.NOT_FOUND,
                    );
                } else {
                    throw error
                }
        }
    }

    idEvents(id: string) {
        try{
            return this.eventsRepository.findOneOrFail({
                where: {id}
            })
        }catch(err) {
            throw new err
        }
    }

    eventsUserId(id: string) {
        return this.eventsRepository.find({
            where: {
                user: {id}
            }
        })
    }

    eventsActive() {
        return this.eventsRepository.find({
            where: {
                event_status: 'Active'
            }
        })
    }

    eventsHistory() {
        return this.eventsRepository.find({
            where: {
                event_status: 'Done'
            },
        })
    }

    async eventsTable(id: string) {
        const event = await this.eventsRepository.find({
            relations: {
                ticket: {
                    order: true
                }
            },
            where: {
                user: {id}
            }
        })
        const arr = []
        event.map(result =>{ 
            const quantity = []
            const ticket = []
            if(result.ticket.length >= 1) {
                result.ticket.map((re) => {
                    ticket.push(re.ticket_category)
                    if(re.order.length >= 1){
                        re.order.map((sum) => quantity.push(sum.quantity))
                    }
                })
            }
            const sum = quantity.reduce((a, b) => a + b, 0)
            arr.push({
            event_id: result.id,
            event_name: result.event_name,
            event_date: result.event_date,
            event_venue: result.event_venue,
            event_status: result.event_status,
            quantity: sum,
            ticket: ticket
        })})
        return arr
    }

    countAll() {
        return this.eventsRepository.count()
    }

    async eventsUpdate(id: string, eventsUpdateDto: EventsUpdateDto) {
        await this.eventsRepository.update({id}, eventsUpdateDto)
        return this.eventsRepository.findOneOrFail({
            where: {id}
        })
    }

    async eventStatusDone(id: string, eventsStatusDto: EventsStatusDto) {
        await this.eventsRepository.update({id}, eventsStatusDto)
        return this.eventsRepository.findOneOrFail({
            where: {id}
        })
    }

    async excel(id: string) {
        const event =  await this.eventsRepository.find({
            relations: {
                ticket: {
                    order: true
                }
            },
            where: {
                user: {id}
            }
        })
        const arr = []
        event.map(result => {
            const order = []
            result.ticket.map(rslt => {
                rslt.order.map(total => order.push(total.quantity))
            })
            const sum = order.reduce((a, b) => a + b, 0)
            arr.push({
                name: result.event_name,
                date: result.event_date,
                venue: result.event_venue,
                ticketsSold: sum,
                status: result.event_status
            })
        })
        return generateExcel(arr, 'events')
    }

    eventDelete(id: string) {
        return this.eventsRepository.softDelete(id)
    }

    async tickets(id: string, ticketsDto: TicketsDto) {
        const event =  await this.eventsRepository.findOneOrFail({
            where: {id}
        })
        const result = new Tickets()
        result.ticket_category = ticketsDto.ticket_category
        result.ticket_price = ticketsDto.ticket_price
        result.total_tickets = ticketsDto.total_tickets
        result.event = event
        await this.ticketsRepository.insert(result)
        return this.ticketsRepository.findOneOrFail({
            where: {
                event: {id}
            }
        })
    }

    ticketEventsId(id:string) {
        return this.ticketsRepository.find({
            where: {
                event: {id}
            }
        })
    }

    async review(id_user: string, id_event: string, reviewsDto: ReviewsDto) {
        const user = await this.usersRepository.findOneOrFail({
            where: {id: id_user}
        })
        const event = await this.eventsRepository.findOneOrFail({
            where: {id: id_event}
        })
        const result = new Reviews()
        result.comment = reviewsDto.comment
        result.rating = reviewsDto.rating
        result.user = user
        result.event = event
        await this.reviewsRepository.insert(result)
        return "Done"
    }

    reviews() {
        return this.reviewsRepository.find()
    }

    reviewsEventsId(id: string) {
        return this.reviewsRepository.find({
            where: {
                event: {id}
            }
        })
    }

    async reviewsRatingEventsId(id: string) {
        const rating = await this.reviewsRepository.find({
            where: {
                event: {id}
            }
        })
        const arr = []
        rating.map(result => arr.push(result.rating))
        const sum = await arr.reduce((a, b) => a + b, 0)
        const length = await arr.length
        const average = await sum / length
        return `average: ${average}`
    }
    

}
