import { BadRequestException, Controller, Get, Param, Post, Request, Res, UploadedFile, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import multer, { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { join } from 'path';
import { Observable, of } from 'rxjs';
import { Portfolio } from 'src/users/entities/portfolio.entity';
import { PaymentsEo } from 'src/payment/entity/paymen.entity';
import { imageFileFilter } from 'helper/fileFilter';

const path = require('path')

export const storage = {
    storage: diskStorage({
        destination: './uploads/portfolio',
        filename: (req, file, cb) => {
            const filename: string = path.parse(file.originalname).name.replace(/\s/g, '') + uuidv4();
            const extension: string = path.parse(file.originalname).ext;
            cb (null, `${filename}${extension}`)
        }
    })
}
export const proofOfPaymentEoStorage = ({
        storage: diskStorage({
                    destination: './uploads/proofOfPaymentsEo',
                    filename: (req, file, cb) => {
                        const filename: string = path.parse(file.originalname).name.replace(/\s/g, '') + uuidv4();
                        const extension: string = path.parse(file.originalname).ext;
                        cb (null, `${filename}${extension}`)
                    }
                }),
      });
      
@Controller('upload')
export class UploadController {
    
    @Post('portfolio')
    @UseInterceptors(FileInterceptor('file', storage))
    uploadFilePortfolio(
        @Request() req,
        @UploadedFile()file
    ): Observable<object> {
        const portfolio: Portfolio = req.portfolio
        return of({filePath: file.filename})
    }

    @Get('portfolio/:filename')
    findFile(
        @Param('filename') filename,
        @Res() res
    ):  Observable<object> {
        return of(res.sendFile(join(process.cwd(), 'uploads/portfolio/' + filename)))
    }

    @Post('eo/payment')
    @UseInterceptors(FileInterceptor('file', proofOfPaymentEoStorage))
    // @UseInterceptors(
    //     FileInterceptor('file', {
    //         fileFilter: imageFileFilter,
    //     })
    // )
    proofOfPaymentEo(
        @Request() req,
        @UploadedFiles() file
    ): Observable<object> {
        const payments: PaymentsEo = req.payments
        return of({imagePath: file.filename})
    }

    @Get('eo/payment/:imagename')
    proofOfPayment(
        @Param('imagename') imagename,
        @Res() res
    ):  Observable<object> {
        return of(res.sendFile(join(process.cwd(), 'uploads/portfolio/' + imagename)))
    }
}
